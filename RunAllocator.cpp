// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <sstream>
#include <vector>
#include <algorithm>
#include "Allocator.hpp"
using namespace std;
// ----
// main
// ----

void check_input(istream& sin, ostream& sout) {

    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    std::string::size_type size;//for int parsing
    string s;
    int numCases = 0;

    //first line guaranteed to be numCases
    //if statement used here for good practice
    if(getline(sin,s)) {
        numCases = std::stoi(s,&size);//TODO cleaner way to do this
    }

    //sout << numCases << "\n" << endl;

    //guaranteed blank line here, so we "eat" it
    getline(sin,s);

    //start of case checking, except we are not sure how many requests are in
    //each one
    for (int i = 0; i < numCases; ++i) {
        //TODO this is where we would set up any necessary data to support
        //correct allocation; DOES not necessarily be implemented with the allocator object
        //but we might want to use it

        allocator_type x;
        int req = 0;
        vector<pointer> ptrs;

        //retrieve allocation requests until blank line or EOF
        while(getline(sin,s) && !s.empty()) {
            //do some processing here, save request as int
            req = std::stoi(s,&size);

            if (req >= 0)
            {
                ptrs.push_back(x.allocate(req));
            }
            else
            {
                int p_index = (-1*req) - 1;
                x.deallocate(ptrs[p_index], 1);
                //cout << "RunAllocator Ptr: " << ptrs[p_index] << "\n" << endl;
                ptrs.erase(ptrs.begin() + p_index);

            }

            std::sort(ptrs.begin(), ptrs.end());

            //sout << req << endl;
        }

        // print
        for (auto block = x.i_begin(); block != x.i_end(); ++block)
        {
            sout << (*block);
            if (block != --x.i_end()) {
                sout << " ";
            }
        }

        //done with this case, so print a blank line if we are not the last one
        sout << "\n";
    }
}

int main () {

    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    the acceptance tests are hardwired to use my_allocator<double, 1000>
    */
    //cout << "-40 944"     << endl;
    //cout << "-40 -24 912" << endl;
    //cout << "40 -24 912"  << endl;
    //cout << "72 -24 880"  << endl;

    check_input(cin,cout);

    /*
    // auto a = x.begin();
    // auto b = x.begin();
    // assert(a == b);

    pointer p = x.allocate(1);
    x.allocate(10);
    // *p = 10;
    //cout << "RunAlloc.cpp: " << p << "\n";
    // cout << *p;
    x.deallocate(p, 1);
    */

    return 0;
}
