// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

/*
TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}
*/


TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}


TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type x;                                            // read/write
    ASSERT_EQ(x[0], 992);
}                                         // fix test

TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 992);
}                                         // fix test


/****************************/
/*     OUR UNITS TESTS      */
/****************************/

// ensure that allocate does not return a null pointer
TEST (AllocatorFixture, unit_test_1)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    const size_type s = 10;
    const pointer p = x.allocate(s);
    
    bool check = p != nullptr;
    ASSERT_EQ(check, true);
}

// allocate one block and make sure first sentinel is correct
TEST (AllocatorFixture, unit_test_2)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;

    allocator_type x;
    const size_type s = 10;
    x.allocate(s);

    ASSERT_EQ(x[0], -40);
}

// allocate one block and make sure last sentinel is correct
TEST (AllocatorFixture, unit_test_3)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;

    allocator_type x;
    const size_type s = 10;
    x.allocate(s);

    ASSERT_EQ(x[44], -40);
}

//allocate one block that will fill whole heap, ensure first sentinel correct
TEST (AllocatorFixture, unit_test_4)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;

    allocator_type x;
    const size_type s = 248;
    x.allocate(s);

    ASSERT_EQ(x[0], -992);
}

//allocate one block that will fill whole heap, ensure first sentinel correct
TEST (AllocatorFixture, unit_test_5)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;

    allocator_type x;
    const size_type s = 248;
    x.allocate(s);

    ASSERT_EQ(x[996], -992);
}

// allocate two block that fill whole heap, ensure start sentinels correct
TEST (AllocatorFixture, unit_test_6)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;

    allocator_type x;
    const size_type s = 123;
    x.allocate(s);
    x.allocate(s);

    bool first_sent = x[0] == -492 && x[500] == -492;

    ASSERT_EQ(first_sent, true);
}

// allocate two block that fill whole heap, ensure end sentinels correct
TEST (AllocatorFixture, unit_test_7)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;

    allocator_type x;
    const size_type s = 123;
    x.allocate(s);
    x.allocate(s);

    bool second_sent = x[496] == -492 && x[996] == -492;

    ASSERT_EQ(second_sent, true);
}

// allocate random blocks, ensure first sentinels correct
TEST (AllocatorFixture, unit_test_8)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;

    allocator_type x;
    const size_type s1 = 1;
    const size_type s2 = 1;
    const size_type s3 = 4;
    const size_type s4 = 2;
    const size_type s5 = 10;

    x.allocate(s1);
    x.allocate(s2);
    x.allocate(s3);
    x.allocate(s4);
    x.allocate(s5);

    bool first_sent = x[0] == -4 && x[12] == -4 && x[24] == -16 && x[48] == -8 && x[64] == -40;

    ASSERT_EQ(first_sent, true);

}

// allocate random blocks, ensure second sentinels correct
TEST (AllocatorFixture, unit_test_9)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;

    allocator_type x;
    const size_type s1 = 1;
    const size_type s2 = 1;
    const size_type s3 = 4;
    const size_type s4 = 2;
    const size_type s5 = 10;

    x.allocate(s1);
    x.allocate(s2);
    x.allocate(s3);
    x.allocate(s4);
    x.allocate(s5);

    bool second_sent = x[8] == -4 && x[20] == -4 && x[44] == -16 && x[60] == -8 && x[108] == -40;

    ASSERT_EQ(second_sent, true);
}

// allocate and deallocate one block
TEST (AllocatorFixture, unit_test_10)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    const size_type s = 1;
    const pointer p1 = x.allocate(s);
    bool before = x[0] == -4;
    x.deallocate(p1, 1);
    bool after = x[0] == 992;

    ASSERT_EQ(before && after, true);
}

// allocate one block that fills whole heap and deallocate
TEST (AllocatorFixture, unit_test_11)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    const size_type s = 248;
    const pointer p1 = x.allocate(s);
    bool before = x[0] == -992 && x[996] == -992;
    x.deallocate(p1, 1);
    bool after = x[0] == 992 && x[996] == 992;

    ASSERT_EQ(before && after, true);
}

// allocate two blocks and remove first block, first block correct
TEST (AllocatorFixture, unit_test_12)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    const size_type s1 = 1;
    const size_type s2 = 1;
    const pointer p1 = x.allocate(s1);
    x.allocate(s2);
    bool before_first = x[0] == -4 && x[8] == -4;
    bool before_second = x[12] == -4 && x[20] == -4;
    x.deallocate(p1, 1);
    bool after_first = x[0] == 4 && x[8] == 4;
    bool after_second = x[12] == -4 && x[20] == -4;

    ASSERT_EQ(before_first && before_second && after_first && after_second, true);
}

// allocate three blocks and deallocate first two, ensure merge
TEST (AllocatorFixture, unit_test_13)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    const size_type s = 1;
    const pointer p1 = x.allocate(s);
    const pointer p2 = x.allocate(s);
    x.allocate(s);
    bool before_first = x[0] == -4 && x[8] == -4;
    bool before_second = x[12] == -4 && x[20] == -4;
    bool before_third = x[24] == -4 && x[32] == -4;
    bool before = before_first && before_second && before_third;
    x.deallocate(p1, 1);
    x.deallocate(p2, 1);
    bool after_first_second = x[0] == 16 && x[20] == 16;
    bool after_third = x[24] == -4 && x[32] == -4;
    bool after = after_first_second && after_third;

    ASSERT_EQ(before && after, true);
}

// allocate random blocks, deallocate random blocks, check intermittent and final sentinels
TEST (AllocatorFixture, unit_test_14)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    const size_type s1 = 10;
    const size_type s2 = 1;
    const size_type s3 = -1;
    const pointer p1 = x.allocate(s1);
    x.allocate(s2);
    x.allocate(s3);
    bool one = x[0] == -40 && x[44] == -40;
    x.deallocate(p1, 1);
    bool two = x[0] == 40 && x[44] == 40;
    const pointer p4 = x.allocate(s1);
    x.allocate(s2);
    x.allocate(s3);
    x.deallocate(p4, 1);
    x.allocate(s1);
    bool three = x[0] == -40 && x[44] == -40;

    ASSERT_EQ(one && two && three, true);

}

// iterate through one block, ensure two iterations
TEST (AllocatorFixture, unit_test_15)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;

    allocator_type x;
    const size_type s = 1;
    x.allocate(s);

    int iters = 0;
    for (auto block = x.i_begin(); block != x.i_end(); block++)
    {
        iters++;   
    }

    ASSERT_EQ(iters, 2);
    //my_allocator<int, 1000>::iterator i = x.i_begin();
}

// iterate through one block that is heap size, ensure two iterations
TEST (AllocatorFixture, unit_test_16)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;

    allocator_type x;
    const size_type s = 248;
    x.allocate(s);

    int iters = 0;
    for (auto block = x.i_begin(); block != x.i_end(); block++)
    {
        iters++;   
    }

    ASSERT_EQ(iters, 1);
}

// allocate 3 blocks, make sure 4 iterations with iterator
TEST (AllocatorFixture, unit_test_17)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;

    allocator_type x;
    const size_type s = 16;
    x.allocate(s);
    x.allocate(s);
    x.allocate(s);

    int iters = 0;
    for (auto block = x.i_begin(); block != x.i_end(); block++)
    {
        iters++;   
    }

    ASSERT_EQ(iters, 4);
}

// allocate 3 blocks, deallocate first two, ensure 3 iterations because merged
TEST (AllocatorFixture, unit_test_18)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    const size_type s = 16;
    const pointer p1 = x.allocate(s);
    const pointer p2 = x.allocate(s);
    x.allocate(s);
    x.deallocate(p1, 1);
    x.deallocate(p2, 1);

    int iters = 0;
    for (auto block = x.i_begin(); block != x.i_end(); block++)
    {
        iters++;   
    }

    ASSERT_EQ(iters, 3);
}

// allocate 10 blocks, deallocate 10 blocks, make sure one iteration with iterator
TEST (AllocatorFixture, unit_test_19)
{
    using allocator_type = my_allocator<int, 1000>;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    const size_type s = 4;
    const pointer p1 = x.allocate(s);
    const pointer p2 = x.allocate(s);
    const pointer p3 = x.allocate(s);
    const pointer p4 = x.allocate(s);
    const pointer p5 = x.allocate(s);
    const pointer p6 = x.allocate(s);
    const pointer p7 = x.allocate(s);
    const pointer p8 = x.allocate(s);
    const pointer p9 = x.allocate(s);
    const pointer p10 = x.allocate(s);

    x.deallocate(p1, 1);
    x.deallocate(p2, 1);
    x.deallocate(p3, 1);
    x.deallocate(p4, 1);
    x.deallocate(p5, 1);
    x.deallocate(p6, 1);
    x.deallocate(p7, 1);
    x.deallocate(p8, 1);
    x.deallocate(p9, 1);
    x.deallocate(p10, 1);

    int iters = 0;
    for (auto block = x.i_begin(); block != x.i_end(); block++)
    {
        iters++;   
    }

    ASSERT_EQ(iters, 1);
}

TEST(AllocatorFixture, unit_test_20) 
{
    using allocator_type = my_allocator<int, 1000>;

    allocator_type x;
    ASSERT_EQ(x[996], 992);
}  