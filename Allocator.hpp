// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>

class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------
    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return (lhs._p) == (rhs._p);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *(_p);
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
	    
	    // get value of front sentinel, may be negative, so
	    // 	we use its absolute value
            int sentinel_value = (*_p) > 0 ? (*_p) : -1 * (*_p);
            char * c = (char *)(_p);
            // use a char pointer so we can increment sentinel
	    // 	address by single bytes, as opposed to four
	    c += sentinel_value + 2 * sizeof(int); // increment sentinel address by stored val
	    	// plus its own size and the back sentinel's size
            _p = (int *) c;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
	
	    // same idea as above, except that we are interested in the sentinel of
	    // the previous block, bound control is enforced by the allocator object
	    // NOT the user
            char * c = (char *)_p;
	    // c should be at the address of the previous block's sentinel
            c -= sizeof(int);
            int prev_sent = (*((int * ) c)) > 0 ? (*((int * ) c)) : -1 * (*((int * ) c));
	    // c should be at the previous block's first sentinel
            c -= (prev_sent + sizeof(int));
            _p = (int *) c;

            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            //iterator x = *this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return (lhs._p) == (rhs._p);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *(_p);
        }

        // -----------
        // operator ++
        // -----------
        const_iterator& operator ++ () {

            int sentinel_value = (*_p) > 0 ? (*_p) : -1 * (*_p);
            char * c = (char *)(_p);
            c += sentinel_value + 2 * sizeof(int);
            _p = (const int *) c;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {

            char * c = (char *)_p;
            c -= sizeof(int);
            int prev_sent = (*((int * ) c)) > 0 ? (*((int * ) c)) : -1 * (*((int * ) c));
            c -= (prev_sent + sizeof(int));
            _p = (int *) c;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * ensures that each sentinel pair matches, that no two
     * free blocks are next to one another and that we are able to
     * arrive at the end of the allocator properly
     */
    bool valid () const {
	
        bool prevFree = false;
        for (const_iterator block = c_begin(); block != c_end(); ++block) {
            const int firstS = *block;
            if (firstS > 0) {
                if (prevFree) return false;
		prevFree = true;
            } else {prevFree = false;}
            
	    //no matter if block is busy or free, we must always check that both
            //sentines match
            char* c = (char*)(&(*block));
            int absFirstSentinel = (firstS) > 0 ? (firstS) : -1 * (firstS);
	    c += absFirstSentinel + sizeof(int);//change sizeof(int) to SENTINEL_SIZE/2
	    const int secondS = *((const int *)(c)); 
	    if (firstS != secondS) return false;
        }
        return true;
    }

public:
    // -----------
    // constructor
    // -----------

#define SENTINEL_SIZE 8
#define OBJ_SIZE sizeof(T)

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {

    	// bad alloc if not enough space for a single T	    
	if (N < sizeof(T) + (2 * sizeof(int))) throw std::bad_alloc();
        (*this)[0] = N - SENTINEL_SIZE;
        (*this)[N-SENTINEL_SIZE/2] = N - SENTINEL_SIZE;
	iterator i = i_end();
       	assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------
    // Sets both sentinels for a given block to busy

    void negate_sentinels (iterator &block)
    {
        // change last sentinel to a negative
        char * c = (char *) (&(*(block)));

        c += *block + (SENTINEL_SIZE / 2);
        int * p = (int *)(&(*c));
        *p = -1 * (*block);

        // change first sentinel to a negativei
	// must do it last otherwise we would
	// increment c by a negative value
        *block = -1 * (*block);
    }

    // Sets both sentinels for a given block to set_size
    void set_sentinels (iterator &block, int set_size)
    {
        // change first sentineli
	// we must perform this change first to ensure that
	// the address of our second sentinel is correct
        *block = set_size;

        // change last sentinel
        char * c = (char *) (&(*(block)));
        c += *block + (SENTINEL_SIZE / 2);
        int * p = (int *)(&(*c));
        *p = set_size;
    }

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type num_objects) {

        //valid alloc must be at leat 1 (or sizeof(T) bytes)
        if (num_objects < 1)
            throw std::bad_alloc();
	assert(num_objects >= 1);

	// raw amount of bytes needed
        int space_needed = (num_objects * OBJ_SIZE) + SENTINEL_SIZE;

        // look for a free block
        bool block_allocated = false;// if no available block, throw error
        iterator block = i_begin();
        iterator result = block;
        
	for (block; block != i_end() && !block_allocated; ++block)
        {
            // check to see if current block has enough space
            int block_size = *block + SENTINEL_SIZE;// raw bytes available

            if (block_size == space_needed)
            {
		// requested exact amount of space available in this block
                negate_sentinels(block);
		assert(*block < 0 && *block == -1 * (space_needed - SENTINEL_SIZE));
                block_allocated = true;
                result = block;
            }
            else if (block_size > space_needed)
            {
                int space_left = block_size - space_needed;

                //edge case, so we just give the user a little
		// extra space
                if (space_left < SENTINEL_SIZE + OBJ_SIZE)
                {
                    negate_sentinels(block);
                }
                else
                {
                    // setting first section of block to busy
                    *block = space_needed - SENTINEL_SIZE;
                    negate_sentinels(block);
                    // increment so we can check the block that succeeds us,
		    // 	set that to temp
		    ++block;
                    iterator temp = block;
                    // set the sentinels of the new free block created after
		    // 	'block'
		    set_sentinels(temp, space_left - SENTINEL_SIZE);
                    // set block to previous value
		    --block;
		    //make sure block has returned to what it was correctly
		    assert(*block == -1 * (space_needed - SENTINEL_SIZE));
                }
                result = block;
                block_allocated = true;
            }
        }

        //if a block was never found throw an exception
        if (!block_allocated)
        	throw std::bad_array_new_length();
        assert(valid());
	// add one byte because the user should not have access to the sentinel
        return (pointer)(&(*result) + 1);
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     * searches for a block pointing to the same address pointed
     * to by ptr. If address is not found or ptr corresponds to
     * a free block, then pointer must be invalid.
     */
    void deallocate (pointer ptr, size_type num_objects) {

        int* p = (int*)ptr;
        // take one byte off ptr to retrieve first sentinel address
	p -= 1;

        // find block passed in
        iterator block = i_begin();
        for (block; block != i_end(); ++block) {
            if (&(*block) == p) {
                break;
            }
        }

        // throw error if not found of if trying to deallocate free block
        if (block == i_end() || *block > 0)
        {
            throw std::invalid_argument("invalid pointer");
        }
	
	assert (block != i_end() && *block < 0);

        // free current block + set temp to prev block
        set_sentinels(block, *block * -1);
        ++block;
        iterator temp = block;
        --block;

        // if the next block is free and not the end block, merge forward
        if ((block != (--i_end())) && (*(temp) > 0))
        {
            set_sentinels(block, *block + *temp + SENTINEL_SIZE);
	    assert (*block > 0 && *temp > 0);
        }

        //if we are not the begin block
        if (block != i_begin()) {
            // checking prev block now
	    --block;
            temp = block;
            ++block;
            //if prev block is free
            if (*(temp) > 0) {
                //add size to current block and 'delete' previous block
                set_sentinels(temp, *block + *temp + SENTINEL_SIZE);
                block = temp;
		assert (*block > 0);
            }
        }
        assert(valid());
    }
    
    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator i_begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator c_begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator i_end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator c_end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
