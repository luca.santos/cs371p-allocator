# CS371p: Object-Oriented Programming Allocator Repo

* Name: Elizabeth Rodriguez and Luca Santos

* EID: ear3475 and ls42578

* GitLab ID: @elizabeth.amanda.rodriguez and @luca.santos

* HackerRank ID: elizabeth_amand1 and luca_santos

* Git SHA: 827d09158b6c0f08ed7f2f49bb6b410a9f54e6fa

* GitLab Pipelines: https://gitlab.com/luca.santos/cs371p-voting/-/pipelines

* Estimated completion time: 15

* Actual completion time: 29

* Comments: (any additional comments you have)
